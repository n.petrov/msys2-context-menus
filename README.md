# Windows Context Menus

## MSYS2
Adds context menus for MSYS, MinGW32 and MinGW64 bash, grouped under MSYS2 cascading menu. The keys assume a default location of C:\msys64 but can be easily edited.

## CMD
Adds commant prompt option for shift+right click context menu. Adds "open here" and "open as admin" under common Command Prompt cascading menu. 